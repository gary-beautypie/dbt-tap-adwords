{%- macro performance_report_dimensions() -%}

        customerid               as account_id,
        account                   as account_name,

        campaignid               as campaign_id,
        campaign                  as campaign_name,
        campaignstate            as campaign_status,

        adgroupid               as ad_group_id,
        adgroup                  as ad_group_name,
        adgroupstate            as ad_group_status,

        clientname               as client_name

{%- endmacro -%}
