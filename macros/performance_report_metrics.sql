{%- macro performance_report_metrics() -%}

        -- Core Metrics
        clicks                                     as clicks,

        {{ tap_adwords.performance_report_cost('cost') }} as cost,

        currency                                   as currency,

        impressions                                as impressions,
        interactions                               as interactions,

        engagements                                as engagements,
        conversions                                as conversions,

        -- Additional Metrics
        allconv                                   as all_conversions,
        views                                      as video_views,

        activeviewviewableimpressions           as active_view_viewable_impressions,
        activeviewmeasurableimpr                as active_view_measurable_impr,

        {{ tap_adwords.performance_report_cost('activeviewmeasurablecost') }} as active_view_measurable_cost,

        gmailclickstowebsite                    as gmail_clicks_to_website,
        gmailsaves                                as gmail_saves,
        gmailforwards                             as gmail_forwards,

        -- Metrics Required for compliance with RMF requirements
        imprabstop                               as impr_abs_top,

        {{ tap_adwords.performance_report_cost('activeviewavgcpm') }} as active_view_avg_cpm,

        activeviewviewablectr                   as active_view_viewable_ctr,
        activeviewmeasurableimprimpr           as active_view_measurable_impr_impr,
        activeviewviewableimprmeasurableimpr  as active_view_viewable_impr_measurable_impr,
        allconvrate                              as all_conv_rate,
        allconvvalue                             as all_conv_value,

        {{ tap_adwords.performance_report_cost('avgcost') }} as avg_cost,
        {{ tap_adwords.performance_report_cost('avgcpc') }} as avg_cpc,

        avgcpe                                    as avg_cpe,

        {{ tap_adwords.performance_report_cost('avgcpm') }} as avg_cpm,

        avgcpv                                    as avg_cpv,
        avgposition                               as avg_position,
        convrate                                  as conv_rate,
        totalconvvalue                           as total_conv_value,

        {{ tap_adwords.performance_report_cost('costallconv') }} as cost_all_conv,
        {{ tap_adwords.performance_report_cost('costconv') }} as cost_conv,

        costconvcurrentmodel                    as cost_conv_current_model,
        crossdeviceconv                          as cross_device_conv,
        ctr                                        as ctr,
        conversionscurrentmodel                  as conversions_current_model,
        convvaluecurrentmodel                   as conv_value_current_model,
        engagementrate                            as engagement_rate,
        interactionrate                           as interaction_rate,
        interactiontypes                          as interaction_types,
        imprtop                                   as impr_top,
        valueallconv                             as value_all_conv,
        valueconv                                 as value_conv,
        valueconvcurrentmodel                   as value_conv_current_model,
        videoplayedto100                         as video_played_to100,
        videoplayedto25                          as video_played_to25,
        videoplayedto50                          as video_played_to50,
        videoplayedto75                          as video_played_to75,
        viewrate                                  as view_rate,
        viewthroughconv                          as view_through_conv

{%- endmacro -%}
