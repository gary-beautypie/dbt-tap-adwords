with source as (

    select * from {{var('schema')}}.accounts

),

renamed as (

    select

        customerid         as account_id,

        name                as account_name,

        currencycode       as currency_code,
        datetimezone      as date_time_zone,

        canmanageclients  as can_manage_clients,
        testaccount        as test_account

    from source

)

select * from renamed
