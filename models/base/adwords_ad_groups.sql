with source as (

    select * from {{var('schema')}}.ad_groups

),

renamed as (

    select

        -- PK
        id                        as ad_group_id,

        -- FKeys
        _sdc_customer_id::bigint  as account_id,
        campaignid               as campaign_id,

        baseadgroupid          as base_ad_group_id,
        basecampaignid          as base_campaign_id,

        -- Ad Group Info
        campaignname             as campaign_name,
        name                      as ad_group_name,

        adgrouptype             as ad_group_type,
        status                    as ad_group_status

    from source

)

select * from renamed
