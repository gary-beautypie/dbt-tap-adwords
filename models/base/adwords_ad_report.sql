with source as (

    select * from {{var('schema')}}.ad_performance_report

),

renamed as (

    select

        -- Dimensions common in all Performance Reports
        {{ tap_adwords.performance_report_dimensions() }},

        -- Report Segments
        {{ tap_adwords.performance_report_day_segment() }},

        network                             as network,
        device                              as device,

        -- Metrics common in all Performance Reports
        {{ tap_adwords.performance_report_metrics() }},

        -- Additional info about the Ad
        adid                               as ad_id,
        adstate                            as ad_status,
        approvalstatus                     as approval_status,
        adtype                             as ad_type,

        adstrength                         as ad_strength,
        autoappliedadsuggestion          as auto_applied_ad_suggestion,

        finalurl                           as final_url,

        ad                                  as text_ad_headline,
        descriptionline1                   as description_line1,
        descriptionline2                   as description_line2,
        displayurl                         as display_url,

        -- expanded text ad or responsive display ads.
        description                         as description,
        headline1                           as headline1,
        headline2                           as headline2,
        path1                               as path1,

        -- reponsive display ads
        businessname                       as business_name,
        calltoactiontextresponsive      as call_to_action_text_responsive,
        shortheadline                      as short_headline,
        longheadline                       as long_headline,
        promotiontextresponsive           as promotion_text_responsive,

        -- responsive search ads
        responsivesearchadheadlines      as responsive_search_ad_headlines,
        responsivesearchaddescriptions   as responsive_search_ad_descriptions,
        responsivesearchadpath1          as responsive_search_ad_path1,

        -- Gmail ads
        gmailadbusinessname              as gmail_ad_business_name,
        gmailadheadline                   as gmail_ad_headline,
        gmailaddescription                as gmail_ad_description,

        -- Image ads
        imageadname                       as image_ad_name,

        -- Will this nightmare of going through all those types of Ads ever end?

        -- Multi Asset Responsive Display Ads
        businessnamemultiassetresponsivedisplay       as business_name_multi_asset_responsive_display,
        longheadlinemultiassetresponsivedisplay       as long_headline_multi_asset_responsive_display,
        headlinesmultiassetresponsivedisplay           as headlines_multi_asset_responsive_display,
        calltoactiontextmultiassetresponsivedisplay as call_to_action_text_multi_asset_responsive_display,
        promotiontextmultiassetresponsivedisplay      as promotion_text_multi_asset_responsive_display,

        -- Account Timezone and last time this report was updated
        timezone                           as account_time_zone,

        _sdc_report_datetime                as updated_at

    from source

)

select *

from renamed

order by report_date asc
