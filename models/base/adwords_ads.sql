with source as (

    select * from {{var('schema')}}.ads

),

renamed as (

    select

        -- Keys
        _sdc_customer_id::bigint                    as account_id,
        basecampaignid                            as base_campaign_id,
        adgroupid                                 as ad_group_id,

        baseadgroupid                            as base_ad_group_id,

        -- Ad Info
        status                                      as ad_status,

        policysummary['reviewState']                as review_state,
        policysummary['combinedApprovalStatus']    as combined_approval_status,
        policysummary['denormalizedStatus']         as denormalized_status,

        trademarkdisapproved                       as trademark_disapproved

    from source

)

select * from renamed
