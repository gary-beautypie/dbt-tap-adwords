with source as (

    select * from {{var('schema')}}.campaigns

),

renamed as (

    select

        -- PK
        id                                as campaign_id,

        -- FKeys
        _sdc_customer_id::bigint          as account_id,
        basecampaignid                  as base_campaign_id,
        budgetid                         as budget_id,

        -- Campaign Info
        name                              as campaign_name,

        status                            as campaign_status,

        servingstatus                    as serving_status,
        adservingoptimizationstatus    as optimization_status,
        advertisingchanneltype          as advertising_channel_type,
        campaigntrialtype               as trial_type,

        startdate                        as start_at,
        enddate                          as end_at,

        -- JSONB arrays in case we need them to filter by label or setting value
        labels                            as labels,
        settings                          as settings

    from source

)

select * from renamed
