with source as (

    select * from {{var('schema')}}.keywords_performance_report

),

renamed as (

    select

        -- Dimensions common in all Performance Reports
        {{ tap_adwords.performance_report_dimensions() }},

        -- Report Segments
        {{ tap_adwords.performance_report_day_segment() }},

        network                              as network,
        device                               as device,

        keyword                              as keyword,

        -- Metrics common in all Performance Reports
        {{ tap_adwords.performance_report_metrics() }},

        -- Metrics available only in the Keywords Performance Report
        searchabstopis                    as search_abs_top_is,
        searchlostabstopisbudget        as search_lost_abs_top_is_budget,
        searchlosttopisbudget            as search_lost_top_is_budget,
        searchexactmatchis                as search_exact_match_is,
        searchimprshare                    as search_impr_share,
        searchlostabstopisrank          as search_lost_abs_top_is_rank,
        searchlostisrank                  as search_lost_is_rank,
        searchlosttopisrank              as search_lost_top_is_rank,
        searchtopis                        as search_top_is,

        -- Additional info about the Keywords
        keywordid                           as keyword_id,
        keywordstate                        as keyword_status,

        criterionservingstatus             as criterion_serving_status,
        matchtype                           as match_type,
        destinationurl                      as destination_url,

        {{ tap_adwords.performance_report_cost('topofpagecpc') }} as top_of_page_cpc,
        {{ tap_adwords.performance_report_cost('firstpagecpc') }} as first_page_cpc,

        -- Account Timezone and last time this report was updated
        timezone                            as account_time_zone,

        _sdc_report_datetime                 as updated_at

    from source

)

select *

from renamed

order by
  report_date asc,
  keyword asc
